import {
    ADD_ISSUE,
    CLOSE_ISSUE,
    DELETE_ISSUE
} from '../actions/constants'

const initialState = []

function todosReducer(state = initialState, action) {
    switch (action.type) {

        case ADD_ISSUE:
            return [
                ...state,
                action.issue
            ]
        case DELETE_ISSUE:
            return state
                .filter(issue => issue.id != action.id)

        case CLOSE_ISSUE:
            return state
                .map
                    (issue => (issue.id === action.id) ?
                        {...issue, status: 'closed'} : issue)
        default:
            return state
    }
}
export default todosReducer