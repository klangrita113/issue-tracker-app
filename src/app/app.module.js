import angular from 'angular'
import ngRedux from 'ng-redux'
import reduxConfig from './app-redux.config'
import HandleDataService from './handle-data.service'
import submitIssueViewComponent from './submit-issue-view/submit-issue-view.component'
import severityInputComponent from './submit-issue-view/severity-input/severity-input.component'
import descriptionInputComponent from './submit-issue-view/description-input/description-input.component'
import assignmentInputComponent from './submit-issue-view/assignement-input/assignement-input.component'
import addIssueComponent from './submit-issue-view/add-issue-button/add-issue-button.component'
import issueDetailsViewComponent from './issue-details-view/issue-details-view.component'
import closeButtonComponent from './issue-details-view/close-button/close-button.component'
import deleteButtonComponent from './issue-details-view/delete-button/delete-button.component'

export default angular
    .module('JSIssueTracker', [ ngRedux ])
    .component('submitIssue', submitIssueViewComponent)
    .component('severityInput', severityInputComponent)
    .component('descriptionInput', descriptionInputComponent)
    .component('assignmentInput', assignmentInputComponent)
    .component('addIssue', addIssueComponent)
    .component('issueDetailsView', issueDetailsViewComponent)
    .component('closeButton', closeButtonComponent)
    .component('deleteButton', deleteButtonComponent)
    .service('handleDataService', HandleDataService)
    .config(reduxConfig)
