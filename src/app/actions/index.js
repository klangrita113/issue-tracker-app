import Chance from 'chance'

import{ ADD_ISSUE,
        CLOSE_ISSUE,
        DELETE_ISSUE} from "./constants"

export const addIssue = (description, severity, assignment) => ({
    type: ADD_ISSUE,
    issue: {
        id: new Chance().guid(),
        description,
        severity,
        assignment,
        status: 'open'
    }
})

export const closeIssue = id => ({
    type: CLOSE_ISSUE,
    id
})

export const deleteIssue = id => ({
    type: DELETE_ISSUE,
    id
})