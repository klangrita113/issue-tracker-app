import template from './submit-issue-view.template.html'

class controller {
    constructor(handleDataService) {
        this.handleDataService = handleDataService;
    }
}
export default {
        template,
        controller
    }