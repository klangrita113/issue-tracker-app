import template from './description-input.template.html'
    
class controller {
    constructor(handleDataService) {
        this.handleDataService = handleDataService;
    }
}
export default {
        template,
        controller
}