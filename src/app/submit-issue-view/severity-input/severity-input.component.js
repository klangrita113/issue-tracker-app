import template from './severity-input.template.html'

class controller {
    constructor(handleDataService) {
        this.handleDataService = handleDataService;
    }
}

export default {
    template,
    controller
    }