import { addIssue, closeIssue, deleteIssue } from './actions'

export default class HandleDataService {
    constructor($ngRedux) {
        Object.assign(this, {
            $ngRedux,
        })

        this.description = '';
        this.severity = '';
        this.assignment = '';
        this.issue = {};

        $ngRedux.connect(this.mapStateToThis)(this)
    }

    mapStateToThis(state) {
        return {
            issues: state.todos
        }
    }

    addIssue(description, severity, assignment) {
        this.$ngRedux.dispatch(addIssue(description, severity, assignment))
    }

    closeIssue(id) {
        //     this.issues.forEach(issue => {
        //         if (issue.id === id) {
        //             issue.status = 'closed';
        //         }
        //     });
        this.$ngRedux.dispatch(closeIssue(id))
    }

    deleteIssue(id) {
        // this.issues = this.issues.filter(issue => issue.id !== id);
        this.$ngRedux.dispatch(deleteIssue(id))
    }

    get() {
        return this.issues;
    }
}