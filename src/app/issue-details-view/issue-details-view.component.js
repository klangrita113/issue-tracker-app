import template from './issue-details-view.template.html'

class controller {
    constructor(handleDataService) {
        this.handleDataService = handleDataService;
    }
}

export default {
    template,
    controller
}