// libs import
const path              = require( 'path' );
const webpack           = require( 'webpack' );

// webpack plugins import
const HtmlWebpackPlugin = require( 'html-webpack-plugin' );

const commonBaseConfig = {
    entry: [
        'babel-polyfill',
    ],

    output: {
        path: path.resolve( __dirname, 'dist' )
    },

    optimization: {
        splitChunks: {
            chunks: 'all'
        }
    }
};

const commonBasePlugins = {
    plugins: [
        new HtmlWebpackPlugin({
            template: './src/index.html',
            inject: true
        })
    ]
};

module.exports = {
    commonBaseConfig,
    commonBasePlugins
};
