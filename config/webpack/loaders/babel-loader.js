const {
    loader
} = require( 'webpack-partial' );

const babelOptions = {
    babelrc: false,
    presets: [
        'stage-0',
        [
            'env',
            {
                targets: {
                    browsers: [
                        '> 3%',
                        'last 2 versions'
                    ]
                },
                modules: false,
                useBuiltIns: 'entry'
            }
        ]
    ],
    plugins: [
        'transform-class-properties',
        'ng-annotate',
        [
            'transform-object-rest-spread',
            { useBuiltIns: true }
        ],
        [
            'angularjs-annotate',
            { explicitOnly: true }
        ]
    ]
};

const babel = (options = babelOptions) => loader({
    test: /\.js$/,
    exclude: /node_modules/,
    use: {
        loader: 'babel-loader',
        options
    }
});

module.exports = babel